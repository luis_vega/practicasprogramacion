# Practica - 2 - Grado Ciencia de Datos
## Ejercicio-1, Piedra, papel o tijera...
### Autor : Luis Vega Jimenez
#### Grupo CD-1

import random as r

seleccionJugador = True  ### Seteamos una variable en true para entrar en el bucle y salir cuando los valores sean los correctos.

while seleccionJugador:
    jugador1 = input('piedra , papel o tijera').lower().strip()  ### pedimos al usuario una opcion, convertimos a minisculas y quitamos los espacios
    maquina = r.randint(1, 3)
    if (jugador1 == 'piedra' or jugador1 == 'papel' or jugador1 == 'tijera'): ## si es correcta saldra del bucle
        seleccionJugador = False
    else:
        print('Introduce una opcion correcta') ## si es correcta la opcion debera introducirla de nuevo

print(maquina, jugador1)  ## mostramos varables para ver si el programa se ejecuta correctamente

# maquina = 1 => piedra
# maquina = 2 => papel
# maquina = 3 => tijera

if (maquina == 1):
  maquina = 'piedra'  ### PIEDRA
  if (maquina == jugador1):
      print('Empate !!')
  elif (maquina == 'piedra' and jugador1 == 'tijera'):
      print('Maquina gana !!')
  else:
    print('Jugador gana !!')

if (maquina == 2):
  maquina = 'papel'  ### PAPEL
  if (maquina == jugador1):
      print('Empate !!')
  elif (maquina == 'papel' and jugador1 == 'piedra'):
      print('Maquina gana !!')
  else:
    print('Jugador gana !!')

if (maquina == 3):
  maquina = 'tijera'  ### TIJERA
  if (maquina == jugador1):
      print('Empate !!')
  elif (maquina == 'tijera' and jugador1 == 'papel'):
      print('Maquina gana !!')
  else:
    print('Jugador gana !!')





